import { test, expect } from '@playwright/test';

test.describe("Login", () => {
  test('Hacker is blocked', async ({ page }) => {
    await page.goto('/');
    await page.getByLabel('Username').fill('Hacker@gmail.com');
    await page.getByLabel('Password').fill('Password');
    await page.getByRole('button', { name: 'Log In to Sandbox' }).click();
    const error = page.locator("//*[@id='error']");
    await expect(error).toBeVisible();
  });
})

