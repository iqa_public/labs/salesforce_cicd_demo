FROM ruby:3.3

RUN apt update && apt -y install jq

 # Setup SFDX environment variables
ENV CLIURL=https://developer.salesforce.com/media/salesforce-cli/sf/channels/stable/sf-linux-x64.tar.xz
ENV SF_AUTOUPDATE_DISABLE=false
ENV SF_USE_GENERIC_UNIX_KEYCHAIN=true
ENV SF_DOMAIN_RETRY=300
ENV SFDX_DISABLE_APP_HUB=true
ENV SF_LOG_LEVEL=warn
ENV SF_DISABLE_LOG_FILE=true
ENV ROOTDIR=force-app/main/default/
ENV TESTLEVEL=RunLocalTests
ENV SCRATCH_ORG_ALIAS=DreamHouse_Scratch
ENV PACKAGEVERSION=""
 # Install Salesforce CLI
RUN mkdir sfdx
RUN wget -qO- $CLIURL | tar xJ -C sfdx --strip-components 1
RUN "./sfdx/bin/sf"
ENV PATH="${PATH}:/sfdx/bin"
 # Output CLI version and plug-in information
RUN sfdx --version
RUN sfdx plugins --core

# Install ruby gems so save time in pipeline
RUN mkdir /mysuite
WORKDIR /mysuite
COPY Gemfile /mysuite/Gemfile
COPY Gemfile.lock /mysuite/Gemfile.lock
RUN gem install bundler
RUN bundle install
