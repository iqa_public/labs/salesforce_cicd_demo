sfdx force:apex:test:run --target-org $SCRATCH_ORG_ALIAS --wait 10 --result-format human --code-coverage --test-level $TESTLEVEL
# TODO: Figure out how to have both junit and human readable formats in one command
mkdir logs
sfdx force:apex:test:run --target-org $SCRATCH_ORG_ALIAS --wait 10 -r junit --test-level $TESTLEVEL >> logs/apex.xml
