# frozen_string_literal: true

RSpec.describe 'Picklists' do
  LeapSalesforce.objects_to_verify.each do |data_class|
    SoqlEnum.values_for(data_class).each do |picklist|
      it "#{picklist} has not changed values" do
        expect(picklist.values).to match_array data_class.picklist_for(picklist.name)
      end
    end
  end
end
