# frozen_string_literal: true

require_relative 'user_field_names'
# An User object mapping to a SOQL User
class User < SoqlData
  include User::Fields
end
