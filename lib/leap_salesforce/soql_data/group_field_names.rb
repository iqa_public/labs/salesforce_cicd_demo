# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Fields for Group mapped from SOQL Group
class Group < SoqlData
  module Fields
    extend SoqlGlobalObjectData

    # Element for 'Group ID', type 'id'
    soql_element :group_id, 'Id'

    # Element for 'Name', type 'string'
    soql_element :name, 'Name'

    # Element for 'Developer Name', type 'string'
    soql_element :developer_name, 'DeveloperName'

    # Element for 'Related ID', type 'reference'
    soql_element :related_id, 'RelatedId'

    # Element for 'Type', type 'picklist'
    soql_element :type, 'Type'

    # Element for 'Email', type 'email'
    soql_element :email, 'Email'

    # Element for 'Owner ID', type 'reference'
    soql_element :owner_id, 'OwnerId'

    # Element for 'Send Email to Members', type 'boolean'
    soql_element :send_email_to_members, 'DoesSendEmailToMembers'

    # Element for 'Include Bosses', type 'boolean'
    soql_element :include_bosses, 'DoesIncludeBosses'

    # Element for 'Created Date', type 'datetime'
    soql_element :created_date, 'CreatedDate'

    # Element for 'Created By ID', type 'reference'
    soql_element :created_by_id, 'CreatedById'

    # Element for 'Last Modified Date', type 'datetime'
    soql_element :last_modified_date, 'LastModifiedDate'

    # Element for 'Last Modified By ID', type 'reference'
    soql_element :last_modified_by_id, 'LastModifiedById'

    # Element for 'System Modstamp', type 'datetime'
    soql_element :system_modstamp, 'SystemModstamp'
  end
end
