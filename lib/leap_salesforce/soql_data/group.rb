# frozen_string_literal: true

require_relative 'group_field_names'
# An Group object mapping to a SOQL Group
class Group < SoqlData
  include Group::Fields
end
