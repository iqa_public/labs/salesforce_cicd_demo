# frozen_string_literal: true

# Basic factory settings made according to values defined at
# https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_case.htm
FactoryBot.define do
  factory :user do
    trait :all do
      user_id { 'Best to not hard code this' }
      username { Faker::Lorem.paragraph_by_chars(number: 80) }
      last_name { Faker::Lorem.paragraph_by_chars(number: 80) }
      first_name { Faker::Lorem.paragraph_by_chars(number: 40) }
      full_name { Faker::Lorem.paragraph_by_chars(number: 121) }
      company_name { Faker::Lorem.paragraph_by_chars(number: 80) }
      division { Faker::Lorem.paragraph_by_chars(number: 80) }
      department { Faker::Lorem.paragraph_by_chars(number: 80) }
      title { Faker::Lorem.paragraph_by_chars(number: 80) }
      street { 'Content depending on textarea' }
      city { Faker::Lorem.paragraph_by_chars(number: 40) }
      state_province { Faker::Lorem.paragraph_by_chars(number: 80) }
      zip_postal_code { Faker::Lorem.paragraph_by_chars(number: 20) }
      country { Faker::Lorem.paragraph_by_chars(number: 80) }
      latitude { 'Content depending on double' }
      longitude { 'Content depending on double' }
      geocode_accuracy { User::GeocodeAccuracy.sample }
      address { 'Content depending on address' }
      email { 'Content depending on email' }
      auto_bcc { true }
      auto_bcc_stay_in_touch { true }
      stay_in_touch_reminder { true }
      email_sender_address { 'Content depending on email' }
      email_sender_name { Faker::Lorem.paragraph_by_chars(number: 80) }
      email_signature { 'Content depending on textarea' }
      stayin_touch_email_subject { Faker::Lorem.paragraph_by_chars(number: 80) }
      stayin_touch_email_signature { 'Content depending on textarea' }
      stayin_touch_email_note { Faker::Lorem.paragraph_by_chars(number: 512) }
      phone { 'Content depending on phone' }
      fax { 'Content depending on phone' }
      mobile { 'Content depending on phone' }
      my_alias { Faker::Lorem.paragraph_by_chars(number: 8) }
      nickname { Faker::Lorem.paragraph_by_chars(number: 40) }
      user_photo_badge_text_overlay { Faker::Lorem.paragraph_by_chars(number: 80) }
      active { true }
      time_zone { User::TimeZone.sample }
      # Please add UserRole to .leap_salesforce.yml (if it's a table) to create association for UserRole
      locale { User::Locale.sample }
      info_emails { true }
      admin_info_emails { true }
      email_encoding { User::EmailEncoding.sample }
      # Please add Profile to .leap_salesforce.yml (if it's a table) to create association for Profile
      user_type { User::UserType.sample }
      language { User::Language.sample }
      employee_number { Faker::Lorem.paragraph_by_chars(number: 20) }
      # Please add  to .leap_salesforce.yml (if it's a table) to create association for
      # Please add Manager to .leap_salesforce.yml (if it's a table) to create association for Manager
      last_login { 'Content depending on datetime' }
      last_password_change_or_reset { 'Content depending on datetime' }
      created_date { 'Content depending on datetime' }
      # Please add CreatedBy to .leap_salesforce.yml (if it's a table) to create association for CreatedBy
      last_modified_date { 'Content depending on datetime' }
      # Please add LastModifiedBy to .leap_salesforce.yml (if it's a table) to create association for LastModifiedBy
      system_modstamp { 'Content depending on datetime' }
      offline_edition_trial_expiration_date { 'Content depending on datetime' }
      sales_anywhere_trial_expiration_date { 'Content depending on datetime' }
      marketing_user { true }
      offline_user { true }
      autologin_to_call_center { true }
      apex_mobile_user { true }
      salesforce_crm_content_user { true }
      knowledge_user { true }
      flow_user { true }
      service_cloud_user { true }
      datacom_user { true }
      sitecom_contributor_user { true }
      sitecom_publisher_user { true }
      workcom_user { true }
      allow_forecasting { true }
      activity_reminders_popup { true }
      event_reminders_checkbox_default { true }
      task_reminders_checkbox_default { true }
      reminder_sound_off { true }
      disable_all_feeds_email { true }
      disable_followers_email { true }
      disable_profile_post_email { true }
      disable_change_comment_email { true }
      disable_later_comment_email { true }
      dis_prof_post_comment_email { true }
      content_no_email { true }
      content_email_as_and_when { true }
      apex_pages_developer_mode { true }
      hide_csn_get_chatter_mobile_task { true }
      disable_mentions_post_email { true }
      dis_mentions_comment_email { true }
      hide_csn_desktop_task { true }
      hide_chatter_onboarding_splash { true }
      hide_second_chatter_onboarding_splash { true }
      dis_comment_after_like_email { true }
      disable_like_email { true }
      sort_feed_by_comment { true }
      disable_message_email { true }
      jigsaw_list_user { true }
      disable_bookmark_email { true }
      disable_share_post_email { true }
      enable_auto_sub_for_feeds { true }
      disable_file_share_notifications_for_api { true }
      show_title_to_external_users { true }
      show_manager_to_external_users { true }
      show_email_to_external_users { true }
      show_work_phone_to_external_users { true }
      show_mobile_phone_to_external_users { true }
      show_fax_to_external_users { true }
      show_street_address_to_external_users { true }
      show_city_to_external_users { true }
      show_state_to_external_users { true }
      show_postal_code_to_external_users { true }
      show_country_to_external_users { true }
      show_profile_pic_to_guest_users { true }
      show_title_to_guest_users { true }
      show_city_to_guest_users { true }
      show_state_to_guest_users { true }
      show_postal_code_to_guest_users { true }
      show_country_to_guest_users { true }
      disable_feedback_email { true }
      disable_work_email { true }
      pipeline_view_hide_help_popover { true }
      hide_s1_browser_ui { true }
      disable_endorsement_email { true }
      path_assistant_collapsed { true }
      cache_diagnostics { true }
      show_email_to_guest_users { true }
      show_manager_to_guest_users { true }
      show_work_phone_to_guest_users { true }
      show_mobile_phone_to_guest_users { true }
      show_fax_to_guest_users { true }
      show_street_address_to_guest_users { true }
      lightning_experience_preferred { true }
      preview_lightning { true }
      hide_end_user_onboarding_assistant_modal { true }
      hide_lightning_migration_modal { true }
      hide_sfx_welcome_mat { true }
      hide_bigger_photo_callout { true }
      global_nav_bar_wt_shown { true }
      global_nav_grid_menu_wt_shown { true }
      create_lex_apps_wt_shown { true }
      favorites_wt_shown { true }
      record_home_section_collapse_wt_shown { true }
      record_home_reserved_wt_shown { true }
      favorites_show_top_favorites { true }
      exclude_mail_app_attachments { true }
      suppress_task_sfx_reminders { true }
      suppress_event_sfx_reminders { true }
      preview_custom_theme { true }
      has_celebration_badge { true }
      user_debug_mode_pref { true }
      new_lightning_report_run_page_enabled { true }
      association :contact_id, factory: :contact
      association :account_id, factory: :account
      # Please add  to .leap_salesforce.yml (if it's a table) to create association for
      extension { 'Content depending on phone' }
      saml_federation_id { Faker::Lorem.paragraph_by_chars(number: 512) }
      about_me { 'Content depending on textarea' }
      url_for_fullsized_photo { 'Content depending on url' }
      photo { 'Content depending on url' }
      show_external_indicator { true }
      out_of_office_message { Faker::Lorem.paragraph_by_chars(number: 40) }
      url_for_medium_profile_photo { 'Content depending on url' }
      chatter_email_highlights_frequency { User::ChatterEmailHighlightsFrequency.sample }
      default_notification_frequency_when_joining_groups { User::DefaultNotificationFrequencywhenJoiningGroups.sample }
      datacom_monthly_addition_limit { 'Content depending on int' }
      last_viewed_date { 'Content depending on datetime' }
      last_referenced_date { 'Content depending on datetime' }
      url_for_banner_photo { 'Content depending on url' }
      url_for_ios_banner_photo { 'Content depending on url' }
      url_for_android_banner_photo { 'Content depending on url' }
      has_profile_photo { true }
    end
  end
end
