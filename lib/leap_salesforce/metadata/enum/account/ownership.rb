# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Account, Ownership
class Account < SoqlData
  # Enumeration for Ownership
  module Ownership
    include SoqlEnum

    @public = 'Public'

    @private = 'Private'

    @subsidiary = 'Subsidiary'

    @other = 'Other'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Ownership'
      end

      # @return [Array] List of values for Ownership
      def values
        %w[Public Private Subsidiary Other]
      end

      attr_reader :public, :private, :subsidiary, :other
    end
  end
end
