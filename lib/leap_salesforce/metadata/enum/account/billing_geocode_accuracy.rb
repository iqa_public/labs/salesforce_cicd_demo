# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Account, BillingGeocodeAccuracy
class Account < SoqlData
  # Enumeration for BillingGeocodeAccuracy
  module BillingGeocodeAccuracy
    include SoqlEnum

    @address = 'Address'

    @near_address = 'Near Address'

    @block = 'Block'

    @street = 'Street'

    @extended_zip = 'Extended Zip'

    @zip = 'Zip'

    @neighborhood = 'Neighborhood'

    @city = 'City'

    @county = 'County'

    @state = 'State'

    @unknown = 'Unknown'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Billing Geocode Accuracy'
      end

      # @return [Array] List of values for BillingGeocodeAccuracy
      def values
        ['Address', 'Near Address', 'Block', 'Street', 'Extended Zip', 'Zip', 'Neighborhood', 'City', 'County', 'State', 'Unknown']
      end

      attr_reader :address, :near_address, :block, :street, :extended_zip, :zip, :neighborhood, :city, :county, :state, :unknown
    end
  end
end
