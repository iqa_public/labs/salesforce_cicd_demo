# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Account, AccountRating
class Account < SoqlData
  # Enumeration for AccountRating
  module AccountRating
    include SoqlEnum

    @hot = 'Hot'

    @warm = 'Warm'

    @cold = 'Cold'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Account Rating'
      end

      # @return [Array] List of values for AccountRating
      def values
        %w[Hot Warm Cold]
      end

      attr_reader :hot, :warm, :cold
    end
  end
end
