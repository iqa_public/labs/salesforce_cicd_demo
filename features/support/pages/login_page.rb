# frozen_string_literal: true

# Page object representing the login page in Salesforce
class LoginPage
  # Use the PageObject module in this class
  include PageObject
  include PageObject::PageFactory

  text_field(:username, id: 'username')
  text_field(:password, id: 'password')

  link(:existing_id, id: 'use_new_identity')

  button(:login, id: 'Login')

  div(:error_message, id: 'error')
end
