# frozen_string_literal: true

Before('@ui') do |scenario|
  require 'watir'
  args = { timeout: 120,
           url: ENV['WEBDRIVER_URL'], name: scenario.name,
           'goog:chromeOptions': {
             args: ENV['WEBDRIVER_CHROMEOPTIONS']&.split(' ') || %w[]
           } }
  @browser = Watir::Browser.new :chrome, options: args
end

After('@ui') do
  @browser.close
  sleep 10 # Time for video to process
end

After do
  @object.delete if @object && !@object.error_message?
end
