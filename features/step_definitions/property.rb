# frozen_string_literal: true
Given('I am a {string}') do |_person|
  # Placeholder
end

When('I create a property without a state') do
  @object = FactoryBot.create(:property, :no_state)
end

Given 'I set the state to {string}' do |state_value|
  @object.state = state_value
end

Then('it is created successfully') do
  expect(@object.successful?).to be true
end

And('I do not set the state') do
  @object.unset = :State__c
end
